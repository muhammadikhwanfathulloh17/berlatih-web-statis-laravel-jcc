<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Registration(){
        return view('form');
    }

    public function Welcome(Request $request){
        $user = [
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
        ];
        return view('welcome', compact('user'));
    }
}
