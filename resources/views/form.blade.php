<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Buat Account</title>
</head>
<body>
    <h2>Buat Account Baru</h2>

    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First name</label><br>
        <input type="text" name="firstname" id="firstname"><br><br>
        <label for="lastname">Last name</label><br>
        <input type="text" name="lastname" id="lastname"><br><br>
        <label for="gender">Gender</label><br>
        <input type="radio" name="male" id="Male">Male<br>
        <input type="radio" name="female" id="Female">Female<br><br>
        <label for="nationality">Nationality</label><br>
        <select name="nationality" id="nationality">
            <option value="Indonesia">Indonesia</option>
            <option value="Amerika">Amerika</option>
            <option value="Inggris">Inggris</option>
        </select><br><br>
        <label for="language">Language Spoken</label><br>
        <input type="checkbox" name="Indonesian" id="Indonesian">Indonesian<br>
        <input type="checkbox" name="English" id="English">English<br>
        <input type="checkbox" name="Other" id="Other">Other<br><br>
        <label for="bio">Bio</label><br>
        <textarea name="description" id="description" cols="30" rows="10"></textarea>
        <br><br>
        <input type="submit" value="Submit">
    </form>
</body>
</html>
